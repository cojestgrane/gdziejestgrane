import os
import platform
import django
# bez dolnych linijek nie dostaniemy się do django i nie zadziałają importy!
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gdziejestgrane.settings")
django.setup()
from accounts.models import User
from events.models import (
    Event,
    Participation,
    SPORT_TYPES
)
from localizations.models import (
    Localization,
    Comment
)
from groups.models import (
    Group,
    Membership
)
import glob
import random
import datetime as dt


import logging
log = logging.getLogger(__name__)
handler = logging.FileHandler(filename='dummy.log', mode='w')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)
log.setLevel(logging.DEBUG)

# Czy korzystasz z venv?
VENV = False
# Wykrywanie systemu i venv
current_directory = os.getcwd()
venv_dir = os.path.join(current_directory, 'venv')
venv = ''

if platform.system() == 'Linux':
    log.debug('Wykryłem Linuxa')
    sep = '/'
    venv = os.path.join(venv_dir, 'bin', 'python') if VENV else 'python'
else:
    log.debug('Wykryłem Windowsa')
    sep = '\\'
    venv = os.path.join(venv_dir, 'Scripts', 'python.exe') if VENV else 'python.exe'


# Usuwanie migracji i pycache
if os.path.basename(current_directory) == 'gdziejestgrane':
    log.debug(f'VENV - {venv_dir}')

    log.debug('Usuwam CACHE')
    for file in glob.glob(f'{current_directory}{sep}**{sep}*.pyc', recursive=True):

        condition = not os.path.dirname(file).startswith(venv_dir)
        log.debug(f"{condition}  {os.path.dirname(file)}")

        if condition:
            log.debug(f'Usuwam {file}')
            os.remove(file)

    log.debug('Usuwam MIGRACJE')
    for file in glob.glob(f'{current_directory}{sep}**{sep}migrations{sep}**', recursive=True):

        condition = not os.path.dirname(file).startswith(venv_dir)
        log.debug(f"{condition}  {os.path.dirname(file)}")

        if os.path.basename(file) != "__init__.py" and condition:
            try:
                os.remove(file)
                log.debug(f'Usuwam {file}')
            except (IsADirectoryError, PermissionError) as e:
                pass

        else:
            log.debug(f'Oszczędziłem {file}')

else:
    log.debug(os.path.basename(current_directory))

# # Usuwanie bazy danych
try:
    os.remove(os.path.join(current_directory, 'db.sqlite3'))
except FileNotFoundError:
    pass

###################################################
# Migracje
log.debug('Zaczynam migracje')
os.system(f'{venv} manage.py makemigrations')
os.system(f'{venv} manage.py migrate')

####################################################
# Generowanie bazy
log.debug('Generuje bazę')
def random_boolean(percent=30):
    return random.randrange(100) < percent


admin = User.objects.create_user(
    email=f'root@email.com',
    password='Pa55w.rd',
    is_superuser=True,
    is_staff=True
)
admin.save()

for num in range(1, 30):
    user = User.objects.create_user(
        email=f'user{num}@email.com',
        password='Pa55w.rd'
    )
    user.save()
users = User.objects.all()
log.debug(users)

for num in range(1, 10):
    Localization.objects.create(
        name=f'localization{num}',
        sport_type=random.choice(SPORT_TYPES)[0],
    )
localizations = Localization.objects.all()

for localization in localizations:
    for user in random.sample(list(users), 12):
        localization.likes.add(user)
log.debug(localizations)

for num in range(2, 62):
    sport = random.choice(SPORT_TYPES)[0]
    spots = random.randint(10, 12)
    if spots == 10:
        full = True
    else:
        full = False
    Event.objects.create(
        name=f'event{num}',
        spots=spots,
        spots_taken=10,
        sport_type=sport,
        is_full=full,
        localization=random.choice(localizations.filter(sport_type=sport)),
        date=dt.date(2019, 12, num // 2),
        lock_in_date=dt.date(2019, 12, num // 2) - dt.timedelta(hours=1)

    )
events = Event.objects.all()
log.debug(events)

for num in range(1, 10):
    Group.objects.create(
        name=f'group{num}',
        sport_type=random.choice(Group.sport_types)[1],
    )
groups = Group.objects.all()
log.debug(groups)

for group in groups:
    for user in random.sample(list(users), 10):  # sample nie przyjmuje QuerySet
        Membership.objects.create(
            user_id=user.id,
            group_id=group.id,
            is_admin=random_boolean()
        )
log.debug(Membership.objects.all())

for localization in localizations:
    for user in random.sample(list(users), 10):  # sample nie przyjmuje QuerySet
        Comment.objects.create(
            user_id=user.id,
            localization_id=localization.id,
            content=f'komentarz użytkownika {user.email}',
        )
log.debug(Comment.objects.all())

for event in events:
    for user in random.sample(list(users), 10):  # sample nie przyjmuje QuerySet
        Participation.objects.create(
            user_id=user.id,
            event_id=event.id,
            invite_reason=f'zaproszenie dla {user.email}',
            is_admin=random_boolean()
        )
log.debug(Participation.objects.all())
