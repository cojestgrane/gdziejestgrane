from django.urls import reverse

from accounts.models import User
from accounts.tests import TestCaseWithFixture
from groups.models import Group, Membership
from localizations.models import Localization


class TestGroupCreateView(TestCaseWithFixture):
    def test_get_success_url(self):
        self.login()
        data = {
            'name': 'test_event',
            'sport_type': 'basketball',
        }
        response = self.client.post(
            reverse('groups-create'),
            data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'groups/groups-detail.html')
        self.assertEqual(self.user, Membership.objects.get(id=1).user)


class TestMyGroupListView(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        for i in range(8):
            Group.objects.create(
                name=f'group{i}',
                sport_type=('basketball', 'Koszykówka'),
            )
        for group in Group.objects.all():
            Membership.objects.create(
                user=cls.user,
                group=group
            )
        part = Membership.objects.get(group_id=1)
        part.is_admin = True
        part.save()

    def test_get_context_data(self):
        self.login()
        response = self.client.get(
            reverse('groups-my')
        )

        self.assertTrue(response.context['is_paginated'])
        self.assertFalse(response.context['is_admin_is_paginated'])
        self.assertIn(
            Group.objects.get(id=1),
            response.context['is_admin_object_list']
        )
        self.assertNotIn(
            Group.objects.get(id=2),
            response.context['is_admin_object_list']
        )


class TestGroupDetailView(TestCaseWithFixture):
    def test_post(self):
        self.login()
        response = self.client.post(
            reverse(
                'groups-detail',
                kwargs={'pk': self.group.id}
            )
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/groups/1')
        self.assertEqual(self.user, Membership.objects.get(id=1).user)
        self.assertIsInstance(Membership.objects.get(id=1), Membership)

        response = self.client.post(
            reverse(
                'groups-detail',
                kwargs={'pk': self.group.id}
            ),
            follow=True
        )

        self.assertTemplateUsed(response, 'groups/groups-detail.html')
        self.assertQuerysetEqual(
            Membership.objects.none(),
            Membership.objects.all(),
            transform=lambda x: x
        )


class TestGroupUpdateView(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        Membership.objects.create(
            user=cls.user,
            group=cls.group
        )

    def test_get_success_url(self):
        self.login()
        data = {
            'name': 'test_group',
            'sport_type': 'football',
            'users': [1]
        }
        response = self.client.post(
            reverse(
                'groups-update',
                kwargs={'pk': self.group.id}
            ),
            data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'groups/groups-detail.html')
        self.assertEqual(Group.objects.get(id=1).name, 'test_group')

    def test_context_data(self):
        self.login()
        response = self.client.get(
            reverse(
                'groups-update',
                kwargs={'pk': self.group.id}
            )
        )

        self.assertQuerysetEqual(
            response.context['form'].fields['users'].queryset,
            User.objects.filter(id=2),
            transform=lambda x: x
        )


class TestEventDeleteView(TestCaseWithFixture):
    def test_get_success_url(self):
        self.login()
        self.assertTrue(Group.objects.all())
        response = self.client.post(
            reverse(
                'groups-delete',
                kwargs={'pk': self.group.id}
            ),
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'groups/groups-list.html')
        self.assertFalse(Group.objects.all())
