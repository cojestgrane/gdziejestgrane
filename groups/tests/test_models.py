from accounts.tests import TestCaseWithFixture
from groups.models import Group, Membership


class TestGroup(TestCaseWithFixture):
    def test___str__(self):
        self.assertEqual(str(self.group), 'group')
        self.assertEqual(str(self.group), self.group.name)

    def test_get_absolute_url(self):
        self.login()
        response = self.client.get(self.group.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'groups/groups-detail.html')


class TestMembership(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.membership = Membership.objects.create(
            user=cls.user,
            group=cls.group
        )

    def test_get_absolute_url(self):
        self.login()
        response = self.client.get(self.membership.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'groups/groups-detail.html')
