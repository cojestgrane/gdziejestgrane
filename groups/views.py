import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)

from accounts.models import User
from events.models import Event
from events.views import MyListView
from groups.models import Group, Membership

logger = logging.getLogger(__name__)

logger.debug(__name__)


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    fields = [
        'name',
        'sport_type',
        'picture'
    ]
    template_name = 'groups/groups-form.html'

    def get_success_url(self, *args, **kwargs):
        return reverse(
            'groups-detail',
            kwargs={'pk': self.object.id}
        )

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        logger.debug(self.request.user)
        logger.debug(self.object)
        Membership.objects.create(
            user=self.request.user,
            group=self.object,
            is_admin=True,
            invite_reason=_(f'{self.request.user} stworzył tę grupę')
        )

        return super().form_valid(form)


class GroupListView(ListView):
    model = Group
    template_name = 'groups/groups-list.html'
    ordering = ['date_created']
    paginate_by = 6


class MyGroupListView(LoginRequiredMixin, ListView, MyListView):
    model = Group
    template_name = 'groups/groups-my.html'
    ordering = ['membership__is_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        page_size = 3
        prefixes = ['', 'is_admin_']
        querysets = dict(zip(prefixes, self.get_queryset()))
        for prefix in prefixes:
            context.update(
                self.pagination(
                    prefix,
                    querysets.get(prefix),
                    page_size
                )
            )

        return context

    def get_queryset(self):
        memberships = Membership.objects.filter(
            user_id=self.request.user)
        groups = Group.objects.filter(
            id__in=memberships.values_list('group_id', flat=True))

        is_admin_memberships = memberships.filter(is_admin=True)
        is_admin_groups = groups.filter(
            id__in=is_admin_memberships.values_list('group_id', flat=True))

        return groups.order_by('name'), is_admin_groups.order_by('date_created')


class GroupDetailView(LoginRequiredMixin, DetailView, MyListView):
    model = Group
    template_name = 'groups/groups-detail.html'
    context_object_name = 'group'

    def get_context_data(self, **kwargs):
        user = self.request.user.id

        memberships = Membership.objects.filter(
            group_id=self.object.id).order_by('-date_joined')

        self.object_list = User.objects.filter(
            id__in=memberships.values_list('user_id', flat=True))

        context = super().get_context_data(**kwargs)

        context['events'] = Event.objects.filter(group_id=self.object.id)

        user_membership = memberships.filter(user_id=user).first()
        context['is_member'] = bool(user_membership)
        context['is_admin'] = (user_membership.is_admin
                               if user_membership
                               else False)

        context.update(
            self.pagination(
                "members_",
                self.object_list,
                paginate_by=10
            )
        )
        return context

    def post(self, request, pk, *args, **kwargs):
        group = self.get_object()
        user = request.user.id

        if request.method == 'POST':

            if not Membership.objects.filter(
                    user_id=user, group_id=group.id).exists():

                membership = Membership.objects.create(
                    user_id=user, group_id=group.id)
                membership.save()
                messages.success(
                    request, _(f'Dołączyłeś do grupy { group.name }')
                )

            else:
                Membership.objects.get(
                    user_id=user, group_id=group.id).delete()
                messages.success(
                    request, _(f'Opuściłeś grupę { group.name }')
                )

        return HttpResponseRedirect(
            reverse(
                'groups-detail',
                kwargs={'pk': pk}
            )
        )


class GroupUpdateView(UpdateView, LoginRequiredMixin):
    model = Group
    fields = [
        'name',
        'sport_type',
        'users',
        'picture'
    ]
    template_name = 'groups/groups-update.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        #  view only members of the group
        #  displays all app users by default (entire db)
        memberships = Membership.objects.filter(group_id=self.object.id)
        context['form'].fields["users"].queryset = User.objects.filter(
            id__in=memberships.values_list('user_id', flat=True))

        return context

    def get_success_url(self, *args, **kwargs):
        return reverse(
            'groups-detail',
            kwargs={'pk': self.object.id}
        )


class GroupDeleteView(DeleteView, LoginRequiredMixin):
    model = Group
    template_name = 'groups/groups-delete.html'

    def get_success_url(self, *args, **kwargs):
        return reverse('groups-list')
