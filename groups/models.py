from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from accounts.models import User


class Group(models.Model):
    sport_types = [
        ('football', _('Piłka nożna')),
        ('basketball', _('Koszykówka')),
        ('volleyball', _('Siatkówka')),
        ('tenis', _('Tenis ziemny'))
    ]

    name = models.CharField(
        max_length=128,
        unique=True,
        verbose_name=_('Nazwa')
    )
    sport_type = models.CharField(
        max_length=32,
        choices=sport_types,
        verbose_name=_('Sport')
    )
    date_created = models.DateTimeField(default=timezone.now)
    users = models.ManyToManyField(
        User,
        through='Membership'
    )
    picture = models.ImageField(
        default="group.jpg",
        verbose_name=_('Zdjęcie')
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse(
            'groups-detail',
            kwargs={'pk': self.id}
        )


class Membership(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE
    )
    date_joined = models.DateTimeField(default=timezone.now)
    invite_reason = models.CharField(max_length=64)
    is_admin = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'group')

    def get_absolute_url(self):
        return reverse(
            'groups-detail',
            kwargs={'pk': self.group.id}
        )
