from django.urls import path

from groups.views import (
    GroupCreateView,
    GroupListView,
    MyGroupListView,
    GroupDeleteView,
    GroupUpdateView,
    GroupDetailView
)

urlpatterns = [

    path('new/', GroupCreateView.as_view(), name='groups-create'),
    path('<int:pk>', GroupDetailView.as_view(), name='groups-detail'),
    path('', GroupListView.as_view(), name='groups-list'),
    path('my/', MyGroupListView.as_view(), name='groups-my'),
    path('<int:pk>/update', GroupUpdateView.as_view(), name='groups-update'),
    path('<int:pk>/delete', GroupDeleteView.as_view(), name='groups-delete')

]
