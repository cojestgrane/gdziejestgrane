import datetime as dt
import logging

import django_filters
from bootstrap_datepicker_plus import DatePickerInput, TimePickerInput
from django import forms
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from geopy.distance import distance as geodist

from accounts.models import UserAddress
from .models import Event

logger = logging.getLogger(__name__)


def addresses(request):
    if request is None:
        return UserAddress.objects.none()

    user = request.user
    return user.addresses.all()


class EventFilter(django_filters.FilterSet):
    """ Filtr wydarzeń. """
    #  address nic nie filtruje - zwraca czysty queryset
    #  potrzebny tylko jako zwykly form field
    address = django_filters.ModelChoiceFilter(
        queryset=addresses,
        label=_('Adres użytkownika'),
        method='address_field'
    )
    name = django_filters.CharFilter(
        lookup_expr='contains',
        label=_('Nazwa')
    )
    sport_type = django_filters.ChoiceFilter(
        choices=Event.sport_types
    )
    date = django_filters.DateTimeFilter(
        widget=DatePickerInput(),
        lookup_expr='gte',
        label=_('Data')
    )
    time = django_filters.TimeFilter(
        widget=TimePickerInput(),
        label=_('Godzina'),
        method='time_filter'
    )
    is_full = django_filters.BooleanFilter(
        label=_('Tylko z wolnymi miejscami'),
        widget=forms.CheckboxInput(),
        method='is_full_filter'
    )
    distance = django_filters.NumberFilter(
        method='distance_filter',
        label=_('Odległość (km)'),
        lookup_expr='lte'
    )

    class Meta:
        model = Event
        fields = [
            'name',
            'localization',
            'sport_type',
            'date',
            'time',
            'is_full',
            'distance'
        ]

    def address_field(self, queryset, name, value):
        return queryset

    def time_filter(self, queryset, name, value):
        date = (dt.datetime.strptime(self.data.get('date'), '%m/%d/%Y')
                or dt.datetime.now())
        logger.debug(date.date())
        logger.debug(value)

        logger.debug(queryset)
        events = queryset.exclude(
            Q(date__lt=date.date()) | Q(date=date.date(), time__lt=value)
        )
        logger.debug(events)

        return events

    def is_full_filter(self, queryset, name, value):
        if not value:
            return queryset

        return queryset.filter(is_full=False)

    def distance_filter(self, queryset, name, value):
        user_address_id = self.data.get('address')
        logger.debug(_(f' id = {user_address_id}'))

        if user_address_id == '':
            return queryset

        user_address = self.request.user.addresses.get(
            id=user_address_id)
        logger.debug(user_address)
        user_coords = (user_address.lat, user_address.lon)
        logger.debug(user_coords)
        logger.debug(f'max dist = {value}')
        
        events = []
        for event in queryset:
            try:
                location = (event.localization.localizationaddress.lat,
                            event.localization.localizationaddress.lon)
                distance = geodist(user_coords, location).km
                logger.debug(location)
                logger.debug(f'distance {distance}')
                if distance < value:
                    events.append(event.id)

            except:
                logger.debug(f'{event.localization} has no address')

        return Event.objects.filter(id__in=events)


