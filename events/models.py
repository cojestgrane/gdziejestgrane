from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from accounts.models import User
from groups.models import Group
from localizations.models import Localization

SPORT_TYPES = [
    ('football', 'Piłka nożna'),
    ('basketball', 'Koszykówka'),
    ('volleyball', 'Siatkówka'),
    ('tenis', 'Tenis ziemny')]


def one_day_hence():
    """ Returns the date 24 hours from now. """
    return timezone.now() + timezone.timedelta(days=1)


class Event(models.Model):
    name = models.CharField(
        max_length=64,
        verbose_name=_('Nazwa')
    )
    sport_types = SPORT_TYPES
    sport_type = models.CharField(
        max_length=32,
        choices=sport_types,
        verbose_name=_('Sport')
    )
    spots = models.IntegerField(
        verbose_name=_('Miejsca'),
        default=10
    )
    spots_taken = models.IntegerField(
        verbose_name=_('Miejsca zajęte'),
        default=0
    )
    is_full = models.BooleanField(default=False)
    localization = models.ForeignKey(
        Localization,
        on_delete=models.CASCADE,
        verbose_name=_('Lokalizacja'),
        default=1,
    )
    date = models.DateField(default=one_day_hence)
    time = models.TimeField(default=one_day_hence)
    lock_in_date = models.DateField(default=one_day_hence)
    lock_in_time = models.TimeField(default=one_day_hence)
    group = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        on_delete=models.CASCADE
    )
    picture = models.ImageField(
        default="event.png",
        verbose_name=_('Zdjęcie')
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse(
            'events-detail',
            kwargs={'pk': self.id}
        )


class Participation(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE
    )
    date_joined = models.DateTimeField(default=timezone.now)
    invite_reason = models.CharField(max_length=64)
    is_admin = models.BooleanField(default=False)
    is_participant = models.BooleanField(default=True)

    class Meta:
        unique_together = ('user', 'event')

    def get_absolute_url(self):
        return reverse(
            'events-detail',
            kwargs={'pk': self.event.id}
        )
