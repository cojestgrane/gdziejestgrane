from bootstrap_datepicker_plus import DatePickerInput, TimePickerInput
from django import forms
from django.utils.translation import ugettext_lazy as _

from events.models import Event

sport_types = [
    ('none', '----'),
    ('football', _('Piłka nożna')),
    ('basketball', _('Koszykówka')),
    ('volleyball', _('Siatkówka')),
    ('tenis', _('Tenis ziemny'))
]


class EventForm(forms.ModelForm):
    class Meta(object):
        model = Event
        fields = (
            'name',
            'sport_type',
            'spots',
            'localization',
            'date',
            'time',
            'picture'
        )
        widgets = {
            'date': DatePickerInput(),
            'time': TimePickerInput()
        }
        labels = {
            'date': _('Data'),
            'time': _('Godzina')
        }
