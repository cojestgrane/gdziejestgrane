import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from django.views.generic.list import MultipleObjectMixin
from django_filters.views import FilterView

from accounts.models import User
from groups.models import Group, Membership
from events.filters import EventFilter
from events.forms import EventForm
from events.models import Event, Participation

logger = logging.getLogger(__name__)


class MyListView(MultipleObjectMixin):
    def pagination(self, prefix, qs, paginate_by):
        logger.debug('my_paginacja')
        self.page_kwarg = f'{prefix}page'

        (paginator,
         page,
         queryset,
         is_paginated) = self.paginate_queryset(qs, paginate_by)

        return {
            f'{prefix}paginator': paginator,
            f'{prefix}page_obj': page,
            f'{prefix}is_paginated': is_paginated,
            f'{prefix}object_list': queryset
        }


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    template_name = 'events/events-form.html'
    form_class = EventForm

    def get_success_url(self, *args, **kwargs):
        return reverse(
            'events-detail',
            kwargs={'pk': self.object.id}
        )

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save(commit=False)
        group_id = self.request.resolver_match.kwargs.get('pk')
        if group_id:
            self.object.group = Group.objects.get(id=group_id)
        self.object.save()

        Participation.objects.create(
            user=self.request.user,
            event=self.object,
            is_admin=True,
            invite_reason=_(f'{self.request.user} stworzył tę grupę')
        )
        self.object.spots_taken = 1

        return super().form_valid(form)


class EventListView(LoginRequiredMixin, FilterView):
    model = Event
    template_name = 'events/events-list.html'
    context_object_name = 'events'
    ordering = ['date']
    paginate_by = 6
    filterset_class = EventFilter


class MyEventListView(LoginRequiredMixin, ListView, MyListView):
    model = Event
    template_name = 'events/events-my.html'
    ordering = ['date']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        page_size = 6
        prefixes = ['', 'is_admin_']
        querysets = dict(zip(prefixes, self.get_queryset()))
        for prefix in prefixes:
            context.update(
                self.pagination(
                    prefix,
                    querysets.get(prefix),
                    page_size
                )
            )

        return context

    def get_queryset(self):
        participations = Participation.objects.filter(
            user_id=self.request.user)
        events = Event.objects.filter(
                 id__in=participations.values_list('event_id', flat=True))

        is_admin_participations = participations.filter(is_admin=True)
        is_admin_events = events.filter(
            id__in=is_admin_participations.values_list('event_id', flat=True))

        return events.order_by(*self.ordering), is_admin_events.order_by(*self.ordering)


class EventDetailView(LoginRequiredMixin, DetailView):
    context_object_name = "event"
    model = Event
    template_name = 'events/events-detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user.id
        group = self.object.group

        if (self.object.group
            and not Membership.objects.filter(
                    user_id=user, group_id=group).exists()):

            context['has_perm'] = False

            return context

        participations = Participation.objects.filter(
            event_id=self.object.id)

        participations_list = []
        for p in participations.filter(is_participant=True):

            participations_list.append(
                {
                    'participant': User.objects.get(id=p.user_id),
                    'participation': p
                }
            )

        context['participations'] = participations_list

        context['participants'] = [p['participant'] for p
                                   in context['participations']]

        user_participation = participations.filter(user_id=user).first()

        is_admin = user_participation.is_admin if user_participation else False
        context['is_admin'] = is_admin
        context['has_perm'] = True

        return context

    def post(self, request, pk, *args, **kwargs):
        event = self.get_object()
        user = request.user.id

        participation = Participation.objects.filter(user_id=user,
                                                     event_id=event).first()

        if participation and participation.is_participant:
            if participation.is_admin:
                participation.is_participant = False
                participation.save()

            else:
                participation.delete()

            event.spots_taken -= 1
            event.is_full = False
            messages.success(
                request, _(f'Opuściłeś wydarzenie {event.name}')
            )

        elif not event.is_full:
            if participation and participation.is_admin:
                participation.is_participant = True
                participation.save()

            else:
                Participation.objects.create(user_id=user, event_id=event.id)

            event.spots_taken += 1
            event.is_full = (event.spots_taken == event.spots)
            messages.success(
                request, _(f'Dołączyłeś do wydarzenia {event.name}')
            )

        event.save()

        return HttpResponseRedirect(
            reverse(
                'events-detail',
                kwargs={'pk': pk}
            )
        )


class EventUpdateView(UpdateView, LoginRequiredMixin):
    model = Event
    template_name = 'events/events-update-form.html'
    form_class = EventForm

    def get_success_url(self, *args, **kwargs):
        return reverse(
            'events-detail',
            kwargs={'pk': self.object.id}
        )


class EventDeleteView(DeleteView, LoginRequiredMixin):
    model = Event
    template_name = 'events/events-delete.html'

    def get_success_url(self, *args, **kwargs):
        return reverse('events-list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()

        participations = Participation.objects.filter(event=self.object)
        recipients = list(User.objects.filter(
            id__in=participations.values_list('user', flat=True)))
        recipient_list = [u.email for u in recipients]
        logger.debug(recipient_list)

        send_mail(
            _(f'Wydarzenie {self.object.name} zostało odwołane'),
            _(f'Wydarzenie {self.object.name} zostało odwołane.'
            f'Data wydarzenia: {self.object.date}, {self.object.time}'),
            'events@django.com',
            recipient_list,
            fail_silently=False,
        )

        return super().delete(request, *args, **kwargs)


def get_my_events(request):
    participations = Participation.objects.filter(
        user_id=request.user,
        is_participant=True
    )
    user_events = Event.objects.filter(
        id__in=participations.values_list('event_id', flat=True))

    html = render_to_string(
        'events_calendar.html',
        {'user_events': user_events}
    )

    return HttpResponse(html)




































