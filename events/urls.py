from django.urls import path, include

from events.views import (
    EventCreateView,
    EventListView,
    EventDetailView,
    MyEventListView,
    EventDeleteView,
    EventUpdateView,
    get_my_events
)

urlpatterns = [
    path(
        'new/',
        include([
            path(
                '',
                EventCreateView.as_view(),
                name="events-create"
            ),
            path(
                'group/<int:pk>',
                EventCreateView.as_view(),
                name="events-create-group"
            )
        ])
    ),
    path('<int:pk>', EventDetailView.as_view(), name='events-detail'),
    path('', EventListView.as_view(), name='events-list'),
    path('my/', MyEventListView.as_view(), name='events-my'),
    path('<int:pk>/update', EventUpdateView.as_view(), name='events-update'),
    path('<int:pk>/delete', EventDeleteView.as_view(), name='events-delete'),
    path('get_my_events/', get_my_events, name='get_my_events')

]
