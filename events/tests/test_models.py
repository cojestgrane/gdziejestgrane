from django.test import TestCase
from django.utils import timezone
from freezegun import freeze_time

from accounts.tests import TestCaseWithFixture
from events.models import Participation, one_day_hence


class TestEvent(TestCaseWithFixture):
    def test___str__(self):
        self.assertEqual(str(self.event), 'event')
        self.assertEqual(str(self.event), self.event.name)

    def test_get_absolute_url(self):
        self.login()
        response = self.client.get(self.event.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'events/events-detail.html')


class TestParticipation(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.participation = Participation.objects.create(
            user=cls.user,
            event=cls.event,
        )

    def test_get_absolute_url(self):
        self.login()
        response = self.client.get(self.participation.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'events/events-detail.html')


class TestOneDayHence(TestCase):
    @freeze_time(timezone.now())
    def test_one_day_hence(self):
        self.assertEqual(
            one_day_hence(),
            timezone.now() + timezone.timedelta(days=1)
        )
