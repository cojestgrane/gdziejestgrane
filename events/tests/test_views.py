from django.urls import reverse

from accounts.tests import TestCaseWithFixture
from events.models import Event, Participation
from groups.models import Group
from localizations.models import Localization


class TestEventCreateView(TestCaseWithFixture):
    def test_get_success_url(self):
        self.login()
        data = {
            'name': 'test_event',
            'localization': 1,
            'sport_type': 'basketball',
            'spots': 15,
            'date': '10/5/2019',
            'time': '22:45:32',
            'group': 1
        }
        response = self.client.post(
            reverse(
                'events-create-group',
                kwargs={'pk': self.group.id}
            ),
            data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'events/events-detail.html')


class TestMyEventListView(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        for i in range(8):
            Event.objects.create(
                name=f'event{i}',
                sport_type=('basketball', 'Koszykówka'),
                spots=15,
                localization=cls.localization
            )
        for event in Event.objects.all():
            Participation.objects.create(
                user=cls.user,
                event=event
            )
        part = Participation.objects.get(event=1)
        part.is_admin = True
        part.save()

    def test_get_context_data(self):
        self.login()
        response = self.client.get(
            reverse('events-my')
        )

        self.assertTrue(response.context['is_paginated'])
        self.assertFalse(response.context['is_admin_is_paginated'])
        self.assertIn(
            Event.objects.get(id=1),
            response.context['is_admin_object_list']
        )
        self.assertNotIn(
            Event.objects.get(id=2),
            response.context['is_admin_object_list']
        )


class TestEventDetailView(TestCaseWithFixture):
    def test_get_success_url(self):
        self.login()
        response = self.client.post(
            reverse(
                'events-detail',
                kwargs={'pk': self.event.id}
            ),
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'events/events-detail.html')
        self.assertEqual(1, Event.objects.get(id=1).spots_taken)
        self.assertIsInstance(Participation.objects.get(id=1), Participation)

        response = self.client.post(
            reverse(
                'events-detail',
                kwargs={'pk': self.event.id}
            ),
            follow=True
        )

        self.assertEqual(0, Event.objects.get(id=1).spots_taken)

    def test_has_perm(self):
        self.login()
        response = self.client.post(
            reverse(
                'events-detail',
                kwargs={'pk': self.event2.id}
            ),
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'events/events-detail.html')
        self.assertFalse(response.context['has_perm'])


class TestEventUpdateView(TestCaseWithFixture):
    def test_get_success_url(self):
        self.login()
        data = {
            'name': 'test_event',
            'localization': 1,
            'sport_type': 'football',
            'spots': 15,
            'date': '10/5/2019',
            'time': '22:45:32',
            'group': 1
        }
        response = self.client.post(
            reverse(
                'events-update',
                kwargs={'pk': self.event.id}
            ),
            data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'events/events-detail.html')


class TestEventDeleteView(TestCaseWithFixture):
    def test_get_success_url(self):
        self.login()
        self.assertTrue(Event.objects.all())

        response = self.client.post(
            reverse(
                'events-delete',
                kwargs={'pk': self.event.id}
            ),
            follow=True
        )
        response = self.client.post(
            reverse(
                'events-delete',
                kwargs={'pk': self.event2.id}
            ),
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'events/events-list.html')
        self.assertFalse(Event.objects.all())


class TestGetMyEvents(TestCaseWithFixture):
    def test_get_my_events(self):
        self.login()
        response = self.client.get(
            reverse('get_my_events')
        )

        self.assertTemplateUsed(response, 'events_calendar.html')
