import datetime as dt

from django.db.models import Q
from django.test import RequestFactory
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from accounts.models import UserAddress
from accounts.tests import TestCaseWithFixture
from events.filters import addresses, EventFilter
from events.models import Event
from localizations.models import Localization, LocalizationAddress

SPORT_TYPES = [
    ('football', _('Piłka nożna')),
    ('basketball', _('Koszykówka')),
    ('volleyball', _('Siatkówka')),
    ('tenis', _('Tenis ziemny'))
]


class HelperClass(object):
    def __init__(self, user):
        self._user = user

    @property
    def user(self):
        return self._user


class TestAddresses(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user_address = UserAddress.objects.create(
            city='Opole',
            street='Niemodlińska',
            house_number='79',
            postcode='45-800',
            user=cls.user
        )

    def test_addresses(self):
        request = HelperClass(self.user)
        self.assertQuerysetEqual(
            addresses(request),
            self.user.addresses.all(),
            transform=lambda x: x
        )
        self.assertQuerysetEqual(
            addresses(None),
            UserAddress.objects.none(),
            transform=lambda x: x
        )


class TestEventFilter(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        for num in range(1, 4):
            Localization.objects.create(
                name=f'localization{num}',
                sport_type=SPORT_TYPES[num],
            )
        for num in range(2, 9):
            Event.objects.create(
                name=f'event{num}',
                spots=10 if num % 2 else 11,
                spots_taken=10,
                sport_type=SPORT_TYPES[num//3],
                is_full=num % 2,
                localization=Localization.objects.get(id=(1+(num//3))),
                date=dt.date(2019, 12, num // 2),
                time=(dt.time(hour=10, minute=0, second=0)
                      if num % 2
                      else dt.time(hour=20, minute=0, second=0)
                )
            )
        cls.user_address = UserAddress.objects.create(
            city='Opole',
            street='Alojzego Dambonia',
            house_number='169',
            postcode='45-860',
            user=cls.user,
            lat=50.6618916,
            lon=17.8891016
        )
        cls.localization_address1 = LocalizationAddress.objects.create(
            city='Opole',
            street='Ojca Józefa Czaplaka',
            house_number='45',
            postcode='45-055',
            localization=Localization.objects.get(id=1),
            lat=50.6721646,
            lon=17.9301689
        )
        cls.localization_address2 = LocalizationAddress.objects.create(
            city='Opole',
            street='Bielska',
            house_number='1',
            postcode='45-404',
            localization=Localization.objects.get(id=2),
            lat=50.6706444,
            lon=17.9700986
        )
        cls.f = EventFilter()
        cls.request_factory = RequestFactory()

    def test_is_fill_filter(self):
        self.login()
        not_full_events = self.f.is_full_filter(
            Event.objects.all(),
            'is_full',
            True
        )
        self.assertQuerysetEqual(
            not_full_events.order_by('date'),
            Event.objects.filter(is_full=False).order_by('date'),
            transform=lambda x: x
        )

        all_events = self.f.is_full_filter(
            Event.objects.all(),
            'is_full',
            False
        )
        self.assertQuerysetEqual(
            all_events.order_by('date'),
            Event.objects.all().order_by('date'),
            transform=lambda x: x
        )

    def test_address_field(self):
        self.login()
        all_events = self.f.address_field(
            Event.objects.all(),
            'irrelevant',
            False
        )

        self.assertQuerysetEqual(
            all_events.order_by('date'),
            Event.objects.all().order_by('date'),
            transform=lambda x: x
        )

    def test_distance_filter_valid(self):
        request = self.request_factory.get(
            reverse('events-list')
        )
        request.user = self.user

        f_1 = EventFilter(
            request=request,
            data={
                'address': 1,
                'distance': 4
            }
        )
        f_1.is_valid()

        self.assertQuerysetEqual(
            f_1.filter_queryset(queryset=Event.objects.all()).order_by('date'),
            Event.objects.filter(localization_id=1).order_by('date'),
            transform=lambda x: x
        )

        f_2 = EventFilter(
            request=request,
            data={
                'address': 1,
                'distance': 6
            }
        )
        f_2.is_valid()

        self.assertQuerysetEqual(
            f_2.filter_queryset(queryset=Event.objects.all()).order_by('date'),
            Event.objects.filter(localization_id__lte=2).order_by('date'),
            transform=lambda x: x
        )

    def test_distance_filter_invalid(self):
        request = self.request_factory.get(
            reverse('events-list')
        )
        request.user = self.user

        ff = EventFilter(
            request=request,
            data={
                'address': '',
                'distance': 4
            }
        )
        ff.is_valid()

        self.assertQuerysetEqual(
            ff.filter_queryset(queryset=Event.objects.all()).order_by('date'),
            Event.objects.all().order_by('date'),
            transform=lambda x: x
        )

    def test_time_filter(self):
        request = self.request_factory.get(
            reverse('events-list')
        )
        request.user = self.user

        ff = EventFilter(
            request=request,
            data={
                'date': '12/02/2019',
                'time': '19:00:00'
            }
        )
        ff.is_valid()
        all_events = Event.objects.all()
        events = all_events.exclude(Q(date__lt='2019-12-02') |
                                    Q(date='2019-12-02', time__lt='19:01:00'))

        self.assertQuerysetEqual(
            ff.filter_queryset(queryset=Event.objects.all()).order_by('date'),
            events.order_by('date'),
            transform=lambda x: x
        )

        ff = EventFilter(
            request=request,
            data={
                'date': '12/03/2019',
                'time': '21:00:00'
            }
        )
        ff.is_valid()
        all_events = Event.objects.all()
        events = all_events.exclude(Q(date__lt='2019-12-03') |
                                    Q(date='2019-12-03', time__lt='21:01:00'))

        self.assertQuerysetEqual(
            ff.filter_queryset(queryset=Event.objects.all()).order_by('date'),
            events.order_by('date'),
            transform=lambda x: x
        )


