from django.contrib.auth.views import LogoutView
from django.urls import path

from accounts.views import (
    UserCreateView,
    UserLoginView,
    HomeView,
    UserAddressCreateView,
    UserDetailView,
    profile,
    get_rating
)

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path(
        'register/',
        UserCreateView.as_view(template_name='accounts/registration.html'),
        name='register'
    ),
    path(
        'login/',
        UserLoginView.as_view(template_name='accounts/login.html'),
        name='login'
    ),
    path(
        'logout/',
        LogoutView.as_view(template_name='accounts/logout.html'),
        name='logout'
    ),
    path('profile/', profile, name='profile'),
    path(
        'address/',
        UserAddressCreateView.as_view(),
        name='accounts-address-create'
    ),
    path('<int:pk>', UserDetailView.as_view(), name='user-detail'),
    path('get_rating/<int:pk>', get_rating, name='get_rating')
]
