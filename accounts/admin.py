from django.contrib import admin

from .models import User, UserAddress, Rating

# from .models import Profile

# Register your models here.
admin.site.register(User)
admin.site.register(UserAddress)
admin.site.register(Rating)

# admin.site.register(Profile)
