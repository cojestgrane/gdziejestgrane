from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from accounts.models import User, UserAddress


class RegistrationForm(UserCreationForm):
    error_messages = {
        'password_mismatch': _("Podano różne hasła."),
    }

    class Meta:
        model = User
        fields = {
            'email',
            'picture'
        }

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].label = _("Hasło")
        self.fields['password1'].help_text = mark_safe("""
            Twoje hasło nie może być zbyt podobne do innych danych.</br>
            Twoje hasło musi zawierać przynajmniej 8 znaków.</br>
            Twoje hasło nie może być popularnie używanym hasłem.</br>
            Twoje hasło nie może składać się z samych cyfr.</br>
        """)
        self.fields['password2'].label = _("Potwierdź hasło")
        self.fields['password2'].help_text = _("""
            W celu weryfikacji wprowadź takie same hasło jak powyżej.
        """)

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class UserLoginForm(AuthenticationForm):
    prefix = 'login'
    error_messages = {
        'invalid_login': _("Proszę podać poprawny %(username)s i hasło. "
                           "Wielkość liter ma znaczenie."),
        'inactive': _("Konto jest nieaktywne."),
    }


class UserUpdateForm(forms.ModelForm):

    class Meta:
        model = User
        fields = [
            'nick',
            'email',
            'phone',
            'sports',
            'picture'
        ]

    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.fields['phone'].label = _("Numer telefonu")
        self.fields['sports'].label = _("Ulubione dyscypliny")


class UserAddressForm(forms.ModelForm):

    class Meta:
        model = UserAddress
        fields = [
            'city',
            'street',
            'house_number',
            'postcode'
        ]

    def __init__(self, *args, **kwargs):
        super(UserAddressForm, self).__init__(*args, **kwargs)
        self.fields['street'].label = _("Ulica")
        self.fields['city'].label = _("Miejscowość")
        self.fields['house_number'].label = _("Numer domu")
        self.fields['postcode'].label = _("Kod pocztowy")
