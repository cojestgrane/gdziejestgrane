import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Avg
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_protect
from django.views.generic import CreateView
from django.views.generic import DetailView
from geopy.geocoders import Nominatim

from accounts.forms import RegistrationForm
from accounts.forms import UserUpdateForm, UserLoginForm, UserAddressForm
from accounts.models import User, UserAddress, Rating

geolocator = Nominatim(user_agent='gdziejestgrane')
logger = logging.getLogger(__name__)


class UserAddressCreateView(LoginRequiredMixin, CreateView):
    model = UserAddress
    form_class = UserAddressForm
    template_name = 'accounts/address-form.html'

    def get_success_url(self):
        return reverse('profile')

    def post(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)

        form = UserAddressForm(request.POST)

        if not form.is_valid():
            return self.form_invalid(form)

        user = request.user
        address = form.save(commit=False)
        logger.debug(address)
        location = geolocator.geocode(address)
        logger.debug(location)
        if location is None:
            return self.form_invalid(form)

        address.lat = location.latitude
        address.lon = location.longitude
        user.addresses.add(address, bulk=False)

        return self.form_valid(form)


class UserLoginView(LoginView):
    authentication_form = UserLoginForm


class UserCreateView(SuccessMessageMixin, CreateView):
    model = User
    form_class = RegistrationForm
    success_message = _("Konto zostało utworzone!")


class HomeView(UserCreateView):
    template_name = 'accounts/home.html'


@csrf_protect
def profile(request):
    if request.is_ajax() and request.method == 'POST':
        form = UserUpdateForm(
            request.POST,
            request.FILES,
            instance=request.user
        )
        logger.debug(f'POST {form}')

        if not form.is_valid():
            logger.debug(_('invalid form'))
            html = render(
                request,
                'includes/user_update_form.html',
                {'u_form': form}
            )

            return HttpResponse(html)
        form.save()
        messages.success(request, _(f'Twoje konto zostało zaktualizowane!'))

        return JsonResponse({'redirect': '/accounts/profile/'})

    form = UserUpdateForm(instance=request.user)
    context = {
        'u_form': form,
        'addresses': request.user.addresses.all(),
        'user': User.objects.get(id=request.user.id)
    }

    return render(request, 'accounts/profile.html', context)


class UserDetailView(LoginRequiredMixin, DetailView):
    context_object_name = "user1"
    model = User
    template_name = 'accounts/user-detail.html'

    def post(self, request, pk, *args, **kwargs):
        if request.is_ajax():
            target = User.objects.get(id=pk)
            source = User.objects.get(id=request.user.id)

            logger.debug(request.POST)
            if Rating.objects.filter(source=source, target=target).exists():
                rating = Rating.objects.get(source=source, target=target)
                rating.rate = request.POST['rating']
                rating.save()
                msg = 'updated'

            else:
                Rating.objects.create(
                    source=source,
                    target=target,
                    rate=request.POST['rating']
                )
                msg = _('created')

            data = {
                "message": msg
            }
            target_ratings = Rating.objects.filter(target=target)
            data['avg'] = target_ratings.aggregate(Avg('rate'))['rate__avg']

            return JsonResponse(data)


def get_rating(request, pk):
    """ Load events into calendar. """
    target = User.objects.get(id=pk)
    source = User.objects.get(id=request.user.id)

    rating = Rating.objects.filter(source=source, target=target).first()
    target_ratings = Rating.objects.filter(target=target)
    data = {
        'rating': (rating.rate
                   if rating
                   else 0),
        'avg': (target_ratings.aggregate(Avg('rate'))['rate__avg']
                if target_ratings
                else _('brak ocen'))
    }

    return JsonResponse(data)
