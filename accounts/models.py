from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from accounts.managers import UserManager


# Create your models here.


class User(AbstractUser):
    """ Custom User model. """
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    alphanumeric = RegexValidator(
        r'^[0-9]*$',
        _('Numer telefonu może zawierać wyłącznie cyfry!')
    )
    num_length = RegexValidator(
        r'^\d{9}$',
        _('Numer telefonu musi się składać z 9 cyfr')
    )

    username = None
    nick = models.CharField(
        max_length=100,
        default=''
    )
    email = models.EmailField(
        _("Adres email"),
        unique=True
    )
    phone = models.CharField(
        _("Numer telefonu"),
        validators=[alphanumeric, num_length],
        max_length=9,
        blank=True,
        null=True
    )
    sports = models.CharField(
        _("Sporty"),
        max_length=300,
        default='',
        blank=True
    )
    picture = models.ImageField(
        default="user_photo.png",
        verbose_name=_('Zdjęcie')
    )

    def __str__(self):
        return self.email

    def get_absolute_url(self):
        return reverse('login')


class UserAddress(models.Model):
    """ Users address. """
    city = models.CharField(max_length=20)
    street = models.CharField(max_length=50)
    house_number = models.CharField(max_length=10)
    postcode = models.CharField(max_length=8)
    lat = models.CharField(max_length=8)
    lon = models.CharField(max_length=8)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='addresses'
    )

    def __str__(self):
        return f'{self.street} {self.house_number}, {self.postcode} {self.city}'

    def get_absolute_url(self):
        return reverse('profile')


class Rating(models.Model):
    target = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name=_('Oceniany')
    )
    source = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name=_('Oceniający')
    )
    rate = models.IntegerField(verbose_name=_('Ocena'))
    date_created = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ('target', 'source')
