// user-detail.html
$(document).ready(function () {
    console.log($('#ocena').attr('data-href'))
    $.ajax({
        url: $('#ocena').attr('data-href'),
        method: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log("success")
            var initialRating = data["rating"]
            console.log(initialRating)
            $("input[value='" + initialRating + "']").prop('checked', true);
            updateRating(data['avg'])
        },
        error: function (errorData) {
            console.log("error")
            console.log(errorData[0]['rating'])
        }
    })

})

$(document).ready(function(){
    $(':radio').change(function() {
      ocena = this.value
      console.log('New star rating: ' + ocena)
      $.ajax({
        url: $('form.rating').attr('data-href'),
        method: 'POST',
        data: {
            'rating': ocena
        },
        success: function(data){
          console.log("success")
          console.log(data)
          updateRating(data['avg'])

        },
        error: function(errorData){
          console.log("error")
          console.log(errorData)
        }
      })
    })
})

function updateRating(value){
    $('#ocena').text("Średnia ocena: " + value);
}

// profile.html

$( document ).ready(function() {
    var form = $('#userUpdateForm')
    form.submit(function(event){
      event.preventDefault()
      $.ajax({
        url: $(this).attr("action"),
        method: $(this).attr("method"),
        data: new FormData($(this)[0]),
        processData: false,
        contentType: false,
        success: function(data){
            console.log('form: success')
            console.log(data)
            if (data.redirect) {
        // data.redirect contains the string URL to redirect to
                window.location.href = data.redirect;
            }
            $('#formm').html(data)
        },
        error: function(errorData){
            console.log('error')
            console.log(errorData)

        }
      })

    })
});

