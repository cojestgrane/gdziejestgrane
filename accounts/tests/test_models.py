from django.test import TestCase
from django.utils import timezone

from accounts.models import User, UserAddress, Rating
from accounts.tests import TestCaseWithFixture


class TestUser(TestCaseWithFixture):
    def test___str__(self):
        self.assertEqual(str(self.user), 'user@email.com')
        self.assertEqual(str(self.user), self.user.email)

    def test_get_absolute_url(self):
        response = self.client.get(self.user.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/login.html')


class TestUserAddress(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user_address = UserAddress.objects.create(
            city='Opole',
            street='Niemodlińska',
            house_number='79',
            postcode='45-800'
        )

    def test___str__(self):
        self.assertEqual(
            str(self.user_address),
            'Niemodlińska 79, 45-800 Opole'
        )

        self.assertEqual(
            str(self.user_address),
            f'{self.user_address.street} {self.user_address.house_number},'
            f' {self.user_address.postcode} {self.user_address.city}'
        )

    def test_get_absolute_url(self):
        self.login()
        response = self.client.get(self.user_address.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('accounts/profile.html')


class TestRating(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.source = User.objects.create(
            email='source0@email.com',
            password='password',
            date_joined=timezone.now(),
            phone='997112661'
        )
        cls.target = User.objects.create(
            email='target0@email.com',
            password='password',
            date_joined=timezone.now(),
            phone='997112662'
        )
        cls.rating = Rating.objects.create(
            source=cls.source,
            target=cls.target,
            rate=3,
        )

    def test_update(self):
        self.rating.rate = 5
        self.rating.save()
        self.assertEqual(self.rating.rate, 5)
