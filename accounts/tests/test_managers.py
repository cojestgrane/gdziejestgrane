from django.test import TestCase

from accounts.models import User


class TestUserManager(TestCase):
    def test_create_user(self):
        self.assertRaises(
            ValueError,
            lambda: User.objects.create_user(
                email='',
                password='password'
            )
        )
        self.user = User.objects.create_user(
            email='user@email.com',
            password='password'
        )

        self.assertIsInstance(self.user, User)
        self.user.delete()

    def test_create_superuser(self):
        self.assertRaises(
            ValueError,
            lambda: User.objects.create_superuser(
                email='root@email.com',
                password='password',
                is_staff=False
            )
        )
        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email='root@email.com',
                password='password',
                is_superuser=False
            )

        self.user = User.objects.create_superuser(
            email='user@email.com',
            password='password'
        )

        self.assertIsInstance(self.user, User)
        self.user.delete()
