import json
import random
from urllib.parse import urlencode

from django.test import Client
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from accounts.forms import UserUpdateForm
from accounts.models import User, Rating
from accounts.tests import TestCaseWithFixture
from accounts.views import UserDetailView


class TestUserAddressCreateView(TestCaseWithFixture):
    def test_post(self):
        self.login()

        response = self.client.post(
            reverse('accounts-address-create'),
            {
                'street': 'Alojzego Dambonia',
                'city': 'Opole',
                'house_number': 169,
                'postcode': ''
            }
        )

        self.assertFormError(
            response,
            'form',
            'postcode',
            'This field is required.'
        )

    def test_post_location(self):
        self.login()

        response = self.client.post(
            reverse('accounts-address-create'),
            {
                'street': 'Pruszkowska',
                'city': 'Opole',
                'house_number': 15,
                'postcode': '45-500'
            }
        )
        address = response.context['form'].save(commit=False)

        self.assertNotIn(address, self.user.addresses.all())
        self.assertTrue(response.context['form'].is_valid())

    def test_get_success_url(self):
        self.login()

        response = self.client.post(
            reverse('accounts-address-create'),
            {
                'street': 'Alojzego Dambonia',
                'city': 'Opole',
                'house_number': 169,
                'postcode': '45-850'
            },
            follow=True
        )

        self.assertRedirects(response, '/accounts/profile/')
        self.assertTemplateUsed(response, 'accounts/profile.html')


class TestProfile(TestCaseWithFixture):
    def test_render(self):
        self.login()
        response = self.client.get(
            reverse('profile')
        )

        self.assertTemplateUsed(response, 'accounts/profile.html')
        self.assertIsInstance(response.context['u_form'], UserUpdateForm)
        self.assertQuerysetEqual(
            response.context['addresses'],
            self.user.addresses.all()
        )
        self.assertEqual(response.context['user'], self.user)
        self.assertQuerysetEqual(
            response.context['addresses'],
            self.user.addresses.all()
        )

    def test_ajax_valid(self):
        self.login()
        data = {
            'nick': 'test',
            'email': 'user@email.com',
            'phone': '666555444'
        }
        form = urlencode(data)
        response = self.client.post(
            reverse('profile'),
            form,
            content_type='application/x-www-form-urlencoded',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertJSONEqual(
            str(response.content, encoding='UTF-8'),
            {'redirect': '/accounts/profile/'}
        )

    def test_ajax_invalid(self):
        self.login()
        data = {
            'nick': self.user.nick,
            'email': 'user@email.com',
            'phone': '6665554441123124'
        }
        form = urlencode(data)
        response = self.client.post(
            reverse('profile'),
            form,
            content_type='application/x-www-form-urlencoded',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertFalse(response.context['form'].is_valid())
        self.assertTemplateUsed(response, 'includes/user_update_form.html')


class TestUserDetailView(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.test = User.objects.create_user(
            email='source0@email.com',
            password='password',
            date_joined=timezone.now(),
            phone='997112661'
        )
        cls.target = User.objects.create_user(
            email='target0@email.com',
            password='password',
            date_joined=timezone.now(),
            phone='997112662'
        )
        cls.source = User.objects.create_user(
            email='source@email.com',
            password='password',
            date_joined=timezone.now(),
            phone='997112662'
        )

    def test_get(self):
        response = self.client.get(reverse(
            'user-detail',
            kwargs={'pk': self.test.id}
        ))
        self.assertRedirects(response, '/accounts/login/?next=%2Faccounts%2F1')

        login = self.client.login(
            email='source0@email.com',
            password='password'
        )
        self.assertTrue(login)

        response = self.client.get(reverse(
            'user-detail',
            kwargs={'pk': self.test.id}
        ))
        self.assertEqual(str(response.context['user']), 'source0@email.com')
        self.assertTemplateUsed(response, 'accounts/user-detail.html')
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        login_test = self.client.login(
            email='source0@email.com',
            password='password'
        )
        self.assertTrue(login_test)

        source_client = Client()
        login_source = source_client.login(
            email='source@email.com',
            password='password'
        )
        self.assertTrue(login_source)

        # test create
        rating = urlencode({
            'rating': 4,
        })
        response = self.client.post(
            reverse(
                'user-detail',
                kwargs={'pk': self.target.id}
            ),
            rating,
            content_type='application/x-www-form-urlencoded',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertJSONEqual(
            str(response.content, encoding='UTF-8'),
            {'message': 'created', 'avg': 4}
        )

        # test update
        rating = urlencode({
            'rating': 3,
        })
        response = self.client.post(
            reverse(
                'user-detail',
                kwargs={'pk': self.target.id}
            ),
            rating,
            content_type='application/x-www-form-urlencoded',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        json_content = json.loads(response.getvalue())

        self.assertJSONEqual(
            str(response.content, encoding='UTF-8'),
            {'message': 'updated', 'avg': 3}
        )
        self.assertEqual(
            json_content['message'],
            'updated'
        )

        # test avg
        rating = urlencode({
            'rating': 4,
        })
        response = source_client.post(
            reverse(
                'user-detail',
                kwargs={'pk': self.target.id}
            ),
            rating,
            content_type='application/x-www-form-urlencoded',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertJSONEqual(
            str(response.content, encoding='UTF-8'),
            {'message': 'created', 'avg': 3.5}
        )


class TestGetRating(TestCase):
    @classmethod
    def setUpTestData(cls):
        for i in range(10):
            User.objects.create_user(
                email='source%d@email.com' % i,
                password='password',
                date_joined=timezone.now(),
                phone='997112662'
            )
        cls.target = User.objects.create_user(
            email='target@email.com',
            password='password',
            date_joined=timezone.now(),
            phone='997112662'
        )

    def test_rating(self):
        login = self.client.login(
            email='target@email.com',
            password='password'
        )
        self.assertTrue(login)

        # no ratings
        response = self.client.get(reverse(
            'get_rating',
            kwargs={'pk': self.target.id}
        ))
        json_content = json.loads(response.getvalue())

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_content['avg'], 'brak ocen')
        self.assertJSONEqual(
            str(response.content, encoding='UTF-8'),
            {'rating': 0, 'avg': 'brak ocen'}
        )

        # get ratings
        for user in User.objects.all():
            Rating.objects.create(
                target=self.target,
                source=user,
                rate=random.randint(1, 5)
            )
        response = self.client.get(
            reverse('get_rating',
                    kwargs={'pk': self.target.id})
        )
        json_content = json.loads(response.getvalue())

        self.assertGreaterEqual(json_content['avg'], 0)
        self.assertLessEqual(json_content['avg'], 5)

        for rating in Rating.objects.all():
            rating.delete()
