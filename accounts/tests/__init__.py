from django.test import TestCase

from accounts.models import User
from events.models import Event
from groups.models import Group
from localizations.models import Localization


class TestCaseWithFixture(TestCase):
    fixtures = ['db.json']

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = User.objects.get(id=2)
        cls.event = Event.objects.get(id=1)
        cls.event2 = Event.objects.get(id=2)
        cls.localization = Localization.objects.get(id=1)
        cls.group = Group.objects.get(id=1)
        cls.login = lambda self: self.client.login(
            email=self.user.email,
            password='password'
        )
