from django.test import TestCase
from django.urls import reverse

from accounts.forms import RegistrationForm, UserUpdateForm, UserAddressForm
from accounts.tests import TestCaseWithFixture
from accounts.models import User


class TestRegistrationForm(TestCase):
    def test___init__(self):
        form = RegistrationForm()
        self.assertEqual(form.fields['password1'].label, 'Hasło')
        self.assertEqual(form.fields['password2'].label, 'Potwierdź hasło')
        self.assertEqual(form.fields['password2'].help_text, """
            W celu weryfikacji wprowadź takie same hasło jak powyżej.
        """)
        self.assertEqual(form.fields['password1'].help_text, """
            Twoje hasło nie może być zbyt podobne do innych danych.</br>
            Twoje hasło musi zawierać przynajmniej 8 znaków.</br>
            Twoje hasło nie może być popularnie używanym hasłem.</br>
            Twoje hasło nie może składać się z samych cyfr.</br>
        """)

    def test_save(self):
        data = {
            'email': 'user@email.com',
            'password1': 'Pa55w.rd',
            'password2': 'Pa55w.rd'
        }
        form = RegistrationForm(data=data)
        self.assertTrue(form.is_valid())

        user = form.save()
        self.assertTrue(user)


class TestLoginForm(TestCaseWithFixture):
    def test_login_redirect(self):
        response = self.client.post(
            reverse('login'),
            {
                'login-username': 'user@email.com',
                'login-password': 'password'
            }
        )
        self.assertRedirects(response, '/events/')

    def test_save(self):
        self.user.email = 'testsave@gmail.com'
        self.user.save()

        user = User.objects.get(id=2)
        self.assertIsInstance(user, User)
        self.assertEqual(user.email, 'testsave@gmail.com')


class TestUserUpdateForm(TestCase):
    def test___init__(self):
        form = UserUpdateForm()
        self.assertEqual(form.fields['phone'].label, 'Numer telefonu')
        self.assertEqual(form.fields['sports'].label, 'Ulubione dyscypliny')


class TestUserAddressForm(TestCase):
    def test___init__(self):
        form = UserAddressForm()
        self.assertEqual(form.fields['street'].label, 'Ulica')
        self.assertEqual(form.fields['house_number'].label, 'Numer domu')
        self.assertIn('postcode', form.fields)

    def test_valid_form(self):
        data = {
            'street': 'Alojzego Dambonia',
            'city': 'Opole',
            'house_number': 169,
            'postcode': '45-850'
        }
        form = UserAddressForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {
            'street': 'Alojzego Dambonia',
            'city': 'Opole',
            'house_number': 169,
            'postcode': '123456789'
        }
        form = UserAddressForm(data=data)
        self.assertFalse(form.is_valid())
