from django import forms
from django.utils.translation import ugettext_lazy as _

from localizations.models import Comment, LocalizationAddress


class LocalizationAddressForm(forms.ModelForm):
    class Meta:
        model = LocalizationAddress
        fields = [
            'city',
            'street',
            'house_number',
            'postcode']
        labels = {
            'city': _("Miejscowość"),
            'street': _("Ulica"),
            'house_number': _('Numer domu'),
            'postcode': _('Kod pocztowy')
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']

    content = forms.CharField(
        label=_("Treść komentarza"),
        required=True,
        widget=forms.Textarea(
            attrs={
                'cols': 80,
                'rows': 4,
                'style': 'resize:none;'
            }
        )
    )


