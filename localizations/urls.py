from django.urls import path

from localizations.views import (
    LocalizationCreateView,
    LocalizationListView,
    MyLocalizationListView,
    LocalizationDetailView,
    LocalizationAddressCreateView,
    CommentUpdateView,
    CommentDeleteView
)

urlpatterns = [
    path('new', LocalizationCreateView.as_view(),
         name='localizations-create'),
    path('', LocalizationListView.as_view(),
         name='localizations-list'),
    path('<int:pk>', LocalizationDetailView.as_view(),
         name='localizations-detail'),
    path('my', MyLocalizationListView.as_view(),
         name='localizations-my'),
    path('<int:pk>/address/', LocalizationAddressCreateView.as_view(),
         name='localizations-address-create'),
    path('comment/<int:pk>/update',
         CommentUpdateView.as_view(), name='comment-update'),
    path('comment/<int:pk>/delete',
         CommentDeleteView.as_view(), name='comment-delete')

]
