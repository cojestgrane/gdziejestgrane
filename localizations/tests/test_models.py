from accounts.tests import TestCaseWithFixture
from localizations.models import LocalizationAddress


class TestLocalization(TestCaseWithFixture):
    def test___str__(self):
        self.assertEqual(str(self.localization), 'localization')
        self.assertEqual(str(self.localization), self.localization.name)

    def test_get_absolute_url(self):
        self.login()
        response = self.client.get(self.localization.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'localizations/localizations-detail.html'
        )


class TestLocalizationAddress(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.address = LocalizationAddress.objects.create(
            city='Opole',
            street='Niemodlińska',
            house_number='79',
            postcode='45-800',
            localization=cls.localization
        )
        cls.address.save()

    def test___str__(self):
        self.assertEqual(str(self.address), 'Niemodlińska 79, 45-800 Opole')
        self.assertEqual(
            str(self.address),
            f'{self.address.street} {self.address.house_number},'
            f' {self.address.postcode} {self.address.city}'
        )