from django.urls import reverse

from accounts.models import User
from accounts.tests import TestCaseWithFixture
from localizations.models import Localization, LocalizationAddress, Comment


class TestLocalizationCreateView(TestCaseWithFixture):
    def test_get_success_url(self):
        self.login()
        response = self.client.post(
            reverse('localizations-create'),
            {
                'sport_type': 'basketball',
                'name': 'localization',
                'localization_url': 'https://www.flashscore.pl/'
            },
            follow=True
        )
        self.assertRedirects(response, '/localizations/2')
        self.assertTemplateUsed(
            response,
            'localizations/localizations-detail.html'
        )
        self.assertEqual(response.status_code, 200)


class TestLocalizationAddressCreateView(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.address = Localization.objects.create(
            sport_type='basketball',
            name='localization',
            localization_url='https://www.flashscore.pl/'
        )

    def test_get_success_url(self):
        self.login()
        response = self.client.post(
            reverse(
                'localizations-address-create',
                kwargs={'pk': 1}
            ),
            {
                'street': 'Alojzego Dambonia',
                'city': 'Opole',
                'house_number': 169,
                'postcode': '45-850'
            },
            follow=True
        )

        self.assertRedirects(response, '/localizations/1')
        self.assertTemplateUsed(
            response,
            'localizations/localizations-detail.html'
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            LocalizationAddress.objects.get(id=1).street,
            'Alojzego Dambonia'
        )

    def test_post_form_invalid(self):
        self.login()
        response = self.client.post(
            reverse(
                'localizations-address-create',
                kwargs={'pk': 1}
            ),
            {
                'street': 'Alojzego Dambonia',
                'city': 'Opole',
                'house_number': 169,
                'postcode': ''
            },
            follow=True
        )

        self.assertFalse(response.context['form'].is_valid())


class TestLocalizationDetailView(TestCaseWithFixture):
    def test_post_like(self):
        self.login()
        response = self.client.post(
            reverse(
                'localizations-detail',
                kwargs={'pk': self.localization.id}
            ),
            content_type='application/x-www-form-urlencoded',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertJSONEqual(
            str(response.content, encoding='UTF-8'),
            {'like': True}
        )
        self.assertIn(
            self.localization,
            User.objects.get(id=2).localization_set.all()
        )

        response = self.client.post(
            reverse(
                'localizations-detail',
                kwargs={'pk': self.localization.id}
            ),
            content_type='application/x-www-form-urlencoded',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertJSONEqual(
            str(response.content, encoding='UTF-8'),
            {'like': False}
        )
        self.assertNotIn(
            self.localization,
            User.objects.get(id=1).localization_set.all()
        )

    def test_post_comment(self):
        self.login()
        response = self.client.post(
            reverse(
                'localizations-detail',
                kwargs={'pk': self.localization.id}
            ),
            {'comment-content': 'Komentarz testowy'}
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/localizations/1')
        self.assertEqual(
            Comment.objects.get(id=1).content,
            'Komentarz testowy'
        )

        response = self.client.post(
            reverse(
                'localizations-detail',
                kwargs={'pk': self.localization.id}
            ),
            {'comment-content': ''}
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/localizations/1')
        self.assertFalse(Comment.objects.filter(id=2).exists())


class TestMyLocalizationListView(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.localization.likes.add(cls.user)
        cls.localization.save()

    def test_get_queryset(self):
        self.login()
        response = self.client.get(
            reverse('localizations-my')
        )

        self.assertQuerysetEqual(
            response.context['object_list'],
            self.user.localization_set.all(),
            transform=lambda x: x
        )


class TestCommentUpdateView(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.comment = Comment.objects.create(
            content='Komentarz Testowy',
            user_id=cls.user.id,
            localization_id=cls.localization.id
        )

    def test_get(self):
        self.login()
        response = self.client.get(
            reverse(
                'comment-update',
                kwargs={'pk': self.comment.id}
            ),
            content_type='application/x-www-form-urlencoded',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )

        self.assertTemplateUsed(response, 'localizations/comment_form.html')

    def test_post_comment(self):
        self.login()
        response = self.client.post(
            reverse(
                'comment-update',
                kwargs={'pk': self.comment.id}
            ),
            {'content': 'Komentarz'}
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/localizations/1')
        self.assertEqual(Comment.objects.get(id=1).content, 'Komentarz')


class TestCommentDeleteView(TestCaseWithFixture):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.comment = Comment.objects.create(
            content='Komentarz Testowy',
            user_id=cls.user.id,
            localization_id=cls.localization.id
        )

    def test_post(self):
        self.login()
        response = self.client.post(
            reverse(
                'comment-delete',
                kwargs={'pk': self.comment.id}
            )
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/localizations/1')
        self.assertQuerysetEqual(
            Comment.objects.all(),
            Comment.objects.none(),
            transform=lambda x: x
        )
