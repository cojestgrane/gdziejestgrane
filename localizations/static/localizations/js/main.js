// localizations-detail.html
$(document).ready(function(){

function updateLikes(btn, newCount, verb){
    btn.text(newCount)
}
var likeCountSpan = $("#like-count-span")
var likeButtonForm = $(".like-button-form")
var likeButton = $("#like-button")
var dislikeButton = $("#dislike-button")

likeButtonForm.submit(function(event){
  event.preventDefault();
  var thisForm = $(this)
  var likeCount = parseInt(likeCountSpan.text()) | 0
  console.log(likeCount)
  var addLike = likeCount + 1
  var removeLike = likeCount - 1

  $.ajax({
    url: thisForm.attr("action"),
    method: thisForm.attr("method"),
    data: thisForm.serialize(),
    success: function(data){
      console.log("set like: success")
      console.log(data)
      if (data.like){
        likeCountSpan.text(addLike)
        likeButton.hide()
        dislikeButton.show()
      }
      else {
        likeCountSpan.text(removeLike)
        dislikeButton.hide()
        likeButton.show()
      }

    },
    error: function(errorData){
      console.log("set like: error")
      console.log(errorData)
    }
  })

})
})

$(document).ready(function(){
  $('.edit').click(function () {
    var button = $(this)
    id = button.attr('value')
    $.ajax({
      url: button.attr('data-href'),
      method: 'GET',
      dataType: 'html',
      success: function(data){
        console.log("get_comment_form: success")
        $('#commentForm' + id).html(data)
        console.log(data)
      },
      error: function(errorData){
        console.log("get_comment_form: error")
        console.log(errorData)
      }
    })
  })
})

