from django.apps import AppConfig


class LocalizationsConfig(AppConfig):
    name = 'localizations'
