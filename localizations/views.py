import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from geopy.geocoders import Nominatim

from events.views import MyListView
from localizations.forms import CommentForm
from localizations.forms import LocalizationAddressForm
from localizations.models import Comment
from localizations.models import Localization
from localizations.models import LocalizationAddress

logger = logging.getLogger(__name__)
geolocator = Nominatim(user_agent='gdziejestgrane')


class LocalizationCreateView(LoginRequiredMixin, CreateView):
    model = Localization
    fields = [
        'name',
        'sport_type',
        'localization_url',
        'picture'
    ]
    template_name = 'localizations/localizations-form.html'

    def get_success_url(self, *args, **kwargs):
        return reverse(
            'localizations-detail',
            kwargs={'pk': self.object.id}
        )


class LocalizationAddressCreateView(LoginRequiredMixin, CreateView):
    model = LocalizationAddress
    form_class = LocalizationAddressForm
    template_name = 'localizations/address-form.html'

    def get_success_url(self):
        return reverse(
            'localizations-detail',
            kwargs={'pk': self.object.id}
        )

    def post(self, request, *args, **kwargs):
        form = LocalizationAddressForm(request.POST)

        if not form.is_valid():
            super().post(request)
            return self.form_invalid(form)

        localization = Localization.objects.get(id=kwargs.get('pk'))
        address = form.save(commit=False)
        location = geolocator.geocode(address)
        address.lat, address.lon = location.latitude, location.longitude
        address.localization = localization
        logger.debug(localization)
        logger.debug(address.__dict__)
        address.save()

        return self.form_valid(form)


class LocalizationListView(ListView):
    model = Localization
    template_name = 'localizations/localizations-list.html'
    context_object_name = 'localizations'
    ordering = ['name']
    paginate_by = 6


class LocalizationDetailView(LoginRequiredMixin, DetailView, MyListView):
    context_object_name = "localization"
    model = Localization
    template_name = 'localizations/localizations-detail.html'

    def get_context_data(self, **kwargs):
        self.object_list = Comment.objects.filter(
            localization_id=self.object.id).order_by('-date_created')
        context = super().get_context_data(**kwargs)

        context['c_form'] = CommentForm(prefix='comment')

        context.update(
            self.pagination(
                "comments_",
                self.object_list,
                paginate_by=5
            )
        )

        return context

    def post(self, request, pk, *args, **kwargs):
        localization = self.get_object()
        user = request.user.id

        if request.is_ajax():
            if localization.likes.filter(id=user).exists():
                localization.likes.remove(user)
                like = False

            else:
                localization.likes.add(user)
                like = True

            return JsonResponse({
                "like": like
            })

        comment_form = CommentForm(request.POST, prefix='comment')
        if comment_form.is_valid():
            comment = Comment.objects.create(
                content=comment_form.cleaned_data["content"],
                user_id=user,
                localization_id=localization.id
            )
            comment.save()

            messages.success(request, _(f'Komentarz został utworzony'))

        return HttpResponseRedirect(
            reverse(
                'localizations-detail',
                kwargs={'pk': pk}
            )
        )


class MyLocalizationListView(LoginRequiredMixin, LocalizationListView):
    template_name = 'localizations/localizations-my.html'

    def get_queryset(self):
        return self.request.user.localization_set.all()


class CommentUpdateView(UpdateView, LoginRequiredMixin):
    model = Comment
    fields = ['content']
    template_name = 'localizations/comment-update.html'

    def get_success_url(self, *args, **kwargs):
        return reverse(
            'localizations-detail',
            kwargs={'pk': self.object.localization.id}
        )

    def get(self, request, pk, *args, **kwargs):
        if request.is_ajax():
            form = CommentForm(instance=Comment.objects.get(id=pk))
            html = render(
                request,
                'localizations/comment_form.html',
                {
                    'form': form
                }
            )

            return HttpResponse(html)


class CommentDeleteView(DeleteView, LoginRequiredMixin):
    model = Comment
    template_name = 'localizations/comment-delete.html'

    def get_success_url(self, *args, **kwargs):
        return reverse(
            'localizations-detail',
            kwargs={'pk': self.object.localization.id}
        )
