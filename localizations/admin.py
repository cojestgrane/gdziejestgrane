from django.contrib import admin

from .models import Comment, Localization, LocalizationAddress

# Register your models here.
admin.site.register(Comment)
admin.site.register(Localization)
admin.site.register(LocalizationAddress)
