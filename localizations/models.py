from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from accounts.models import User

SPORT_TYPES = [
    ('football', _('Piłka nożna')),
    ('basketball', _('Koszykówka')),
    ('volleyball', _('Siatkówka')),
    ('tenis', _('Tenis ziemny'))
]


class Localization(models.Model):
    sport_types = SPORT_TYPES
    name = models.CharField(
        max_length=64,
        verbose_name=_('Nazwa')
    )
    sport_type = models.CharField(
        max_length=32,
        choices=sport_types,
        verbose_name=_('Sport')
    )
    localization_url = models.URLField(verbose_name=_('Link'))
    likes = models.ManyToManyField(
        User,
        verbose_name=_('Polubienia')
    )
    picture = models.ImageField(
        default= "localization.png",
        verbose_name=_('Zdjęcie')
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse(
            'localizations-detail',
            kwargs={'pk': self.id}
        )


class Comment(models.Model):
    user = models.ForeignKey(
        User,
        related_name='comments',
        on_delete=models.CASCADE
    )
    localization = models.ForeignKey(
        Localization,
        related_name='comments',
        on_delete=models.CASCADE,
        default=1
    )
    content = models.CharField(
        max_length=1000,
        blank=False,
        null=False
    )
    date_created = models.DateTimeField(default=timezone.now)


class LocalizationAddress(models.Model):
    city = models.CharField(max_length=20)
    street = models.CharField(max_length=50)
    house_number = models.CharField(max_length=10)
    postcode = models.CharField(max_length=8)
    lat = models.CharField(max_length=8)
    lon = models.CharField(max_length=8)
    localization = models.OneToOneField(
        Localization,
        on_delete=models.CASCADE,
        verbose_name=_('Obiekt'),
    )

    def __str__(self):
        return f'{self.street} {self.house_number}, {self.postcode} {self.city}'
